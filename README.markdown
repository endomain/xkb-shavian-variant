# Shavian XKB Variant Keylayout

The intent of this project is to make the Shavian alphabet more accessible
to users of common Linux distributions such as Ubuntu. It is currently a
WORK IN PROGRESS and has *unresolved issues* (see below). 

When these issues are resolved, upon proper installation an English Shavian
keyboard will be as accessible as alternative layouts of english such as 
Dvorak or other international keyboards.

## Usage & Issues

This seems to Just Work with KDE environments and custom environments.
You can use this command to invoke it:

    setxkbmap -layout us -variant shavian

It'll be trickyto type, but you can revert it back to the default
keymap variant by typing:

    setxkbmap -layout us

(Please note if you're not an english speaker, you should choose the localization of your choice for reverting, and please consider contrbituing to this project
with a variant for your dialect!)

Currently this layout doesn't properly work when set via the Ubuntu 
keyboard layout chooser. You'll need to directly use `setxkbmap` with
keyboard shortcuts in Ubuntu to get the effect you want. For example,
I have added the following bindings Super-0 to set to US baseline
in all cases, and Super-1 to jump to Shavian.

## Installation:

Locate on your system where X11 files are provisioned. Many distributions
use `/usr/share/X11/xkb`, but sometimes hide it under `/usr/X11`. Copy
the files from `./symbols` and `./rules` into their analogues, then restart
X11. 

You should now have a variant keyboard available. Under more restrictive
distributions (such as Ubuntu) you will probably need to use the UI tweak tool
to make these visible from the UI.

I've included my default copy of the en and rules files so that in the event you do this and then edit it and screw up (as I did) you can quickly revert it. Because WOW is it a hassle to fix if you didn't back it up. Not that I would ever do such a thing! ;)

## Layout

The current layout offered as seen by Ubuntu's introspection tool:

![shavian qwerty keymap](images/layout.png)

## Credits

Thanks very much to ·𐑡𐑪𐑯 who put in the original work at [Shavian Web](https://shavian.weebly.com/) on this layout.


<p xmlns:dct="http://purl.org/dc/terms/">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://gitlab.com/endomain/xkb-shavian-variant/">
    <span property="dct:title">David Fayram</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Shavian Keyboard Layout</span>.
</p>
